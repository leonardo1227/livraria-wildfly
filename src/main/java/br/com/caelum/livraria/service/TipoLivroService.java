package br.com.caelum.livraria.service;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import br.com.caelum.livraria.dao.TipoLivroDao;
import br.com.caelum.livraria.modelo.TipoLivro;

@Stateless

public class TipoLivroService implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Inject
	TipoLivroDao dao;
	
	public List<TipoLivro> buscaTodos(){
		return dao.buscaTodos();
	}
	
	public void adiciona(TipoLivro tipoLivro){
		dao.adiciona(tipoLivro);
	}

}
