package br.com.caelum.livraria.dao;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.caelum.livraria.modelo.TipoLivro;

@SuppressWarnings("serial")
@Stateless
public class TipoLivroDao implements Serializable{
	
	@PersistenceContext
	EntityManager manager;
	
	private DAO<TipoLivro> dao;
	
	@PostConstruct
	void init(){
		this.dao = new DAO<>(manager, TipoLivro.class);
	}
	
	public void adiciona(TipoLivro tipo){
		dao.adiciona(tipo);
	}
	
	public List<TipoLivro> buscaTodos(){
		return dao.listaTodos();
	}

}
