package br.com.caelum.livraria.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.caelum.livraria.dao.TipoLivroDao;
import br.com.caelum.livraria.modelo.TipoLivro;
import br.com.caelum.livraria.service.TipoLivroService;

@Named
@ViewScoped
public class TipoLivroBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private TipoLivro tipoLivro;
	
	private List<TipoLivro> listaTipoLivro;
	
//	@Inject
//	private TipoLivroDao dao;
	@Inject
	TipoLivroService service;

	public TipoLivro getTipoLivro() {
		return tipoLivro;
	}

	public void setTipoLivro(TipoLivro tipoLivro) {
		this.tipoLivro = tipoLivro;
	}

	public List<TipoLivro> getListaTipoLivro() {
		if(this.listaTipoLivro==null || this.listaTipoLivro.size()==0){
//			this.listaTipoLivro = dao.buscaTodos();
			this.listaTipoLivro = service.buscaTodos();
		}
		return listaTipoLivro;
	}

	public void setListaTipoLivro(List<TipoLivro> listaTipoLivro) {
		this.listaTipoLivro = listaTipoLivro;
	}
	
	public void salvar(){
//		dao.adiciona(tipoLivro);
		service.adiciona(tipoLivro);
	}
	

}
